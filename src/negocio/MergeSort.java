package negocio;

import java.util.ArrayList;
import java.util.Collections;

public class MergeSort <T extends Comparable<T>>
{
	private ArrayList<T> input;
	
	public MergeSort(ArrayList<T> arr) 
	{
		input = arr;
	}
	
	public void mergesort() 
	{
		if(input == null) 
			throw new IllegalArgumentException("Parametro Null");

		if(input.size() > 1)
			sort(0, input.size()-1);
	}
	
	protected void sort(int posIzq, int posDer)
	{	
		int posCentral = posIzq + (posDer - posIzq)/2;
		
		if(posIzq < posDer) {
			sort(posIzq, posCentral);
			sort(posCentral+1, posDer);
			merge(posIzq, posCentral, posDer);
		}
	}
	
	private void merge(int posIzq, int posCentral, int posDer)
	{
		ArrayList<T> auxiliar = new ArrayList<>(Collections.nCopies(input.size(), null));
		
		Collections.copy(auxiliar, input);

		int i = posIzq; 
		int j = posCentral+1;
		int k = posIzq;

		while(i <= posCentral && j <= posDer) 
			{
				if(auxiliar.get(i).compareTo(auxiliar.get(j) ) <= 0) 
					input.set(k, auxiliar.get(i++));
				
				else 
					input.set(k, auxiliar.get(j++));
				k++;
			}
			
		while(i <= posCentral) 
			input.set(k++, auxiliar.get(i++));
	}
	
	public boolean isEmpty()
	{
		if (input.isEmpty())
		{
			return true;
		}
		return false;
	}
}
