package negocio;

import java.util.ArrayList;
import java.util.HashMap;

public class RedTelefonica
{
	private static HashMap <Localidad, ArrayList<ConexionRed>> conexiones;

	public RedTelefonica()
	{
		conexiones = new HashMap <Localidad, ArrayList<ConexionRed>>();
	}

	//"Todos Contra Todos". Este m�todo se encarga de crear las aristas entre todas las localidades contra todas las localidades.
	void TCT()
	{
		for (Localidad origen : conexiones.keySet())
		{
			for (Localidad destino : conexiones.keySet())
			{
				if (!origen.equals(destino))
				{
					agregarConexion(origen, destino);
				}
			}
		}
	}

	public void agregarLocalidad(String nom, String prov, int hab, double lat, double lon)
	{
		conexiones.put(new Localidad(nom, prov, hab, lat, lon), new ArrayList<ConexionRed>());
	}
	
	void agregarConexion(Localidad origen, Localidad destino)
	{
		ConexionRed origen_destino = new ConexionRed(origen, destino);
				
		if(!existeConexion(origen_destino))
		{
			conexiones.get(origen).add(origen_destino);
		}
	}
	
	public boolean existeConexion(ConexionRed arista)
	{	
		return conexiones.containsKey(arista);
	}

	//Getters
	
	//Retorna la Red completa
	public RedTelefonica getRedConexiones()
	{
		return (RedTelefonica) conexiones.clone();
	}
	
	//Retorna una localidad especifica
	public Localidad getLocalidadEspecifica(String nombre)
	{
		Localidad especifica = null;
		for (Localidad localidad : getTodasLocalidades())
		{
			if ( localidad.getNombre().equals(nombre))
			{
				especifica = localidad;
			}
		}
		return especifica;
	}
	
	//Retorna todas las localidades
	public ArrayList<Localidad> getTodasLocalidades()
	{
		ArrayList<Localidad> ret = new ArrayList<>(); 
		for(Localidad localidad: conexiones.keySet())
		{
			ret.add(localidad);
		}
		return ret;
	}
	
	//Retorna todas las conexiones de una localidad especifica
	public ArrayList<ConexionRed> getConexionesLocalidadEspecifica(Localidad localidad)
	{
		return conexiones.get(localidad);
	}
	
	//Retorna todas las conexiones de la Red Telefonica
	public ArrayList<ConexionRed> getTodasConexiones()
	{
		ArrayList<ConexionRed> RedConexiones = new ArrayList<ConexionRed>();
		for (ArrayList<ConexionRed> conexionRed : conexiones.values())
		{
			RedConexiones.addAll(conexionRed);
		}	
		return RedConexiones;
	}

	public int cantLocalidadesTotales()
	{
		return conexiones.size();
	}
	
	public int cantConexionesTotales()
	{
		int cantidad = 0;
		
		for (ArrayList<ConexionRed> listaConexiones : conexiones.values())
		{
			if (listaConexiones.size() != 0)
			{
				cantidad += listaConexiones.size();
			}
		}
		return cantidad;
	}
}