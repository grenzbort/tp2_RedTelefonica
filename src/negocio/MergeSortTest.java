package negocio;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class MergeSortTest
{
	@Test
	public void ArrayOrdenadoTest()
	{
		ArrayList<ConexionRed> array = ordenarArray();
		ArrayList<ConexionRed> esperado = inicializarArrays("Ordenado");

		assertTrue(AuxAssert.ArrayEquals(esperado ,array));		
	}

	@Test
	public void ArrayDesordenadoTest()
	{
		ArrayList<ConexionRed> array = ordenarArray();
		ArrayList<ConexionRed> esperado = inicializarArrays("Desordenado");

		assertFalse(AuxAssert.ArrayEquals(esperado ,array));		
	}

	private ArrayList<ConexionRed> ordenarArray()
	{
		ArrayList<ConexionRed> ret = inicializarArrays("Desordenado");
		MergeSort<ConexionRed> ordenarConexiones = new MergeSort<ConexionRed>(ret);
		ordenarConexiones.mergesort();

		return ret;
	}

	private ArrayList<ConexionRed> inicializarArrays(String tipo)
	{
		ArrayList<ConexionRed> predefinido = new ArrayList<ConexionRed>();

		Localidad localidad1 = new Localidad("Don Torcuato", "Buenos Aires", 10000, -34.49, -58.62);
		Localidad localidad2 = new Localidad ("Rosario", "Santa Fe", 125000, -32.94, -60.63);
		Localidad localidad3 = new Localidad("Bahia Blanca", "Buenos Aires", 250000, -38.71, -62.24);

		ConexionRed conexionA = new ConexionRed (localidad1, localidad2);
		ConexionRed conexionB = new ConexionRed (localidad2, localidad3);
		ConexionRed conexionC = new ConexionRed (localidad3, localidad1);

		if(tipo.equals("Desordenado"))
		{
			predefinido.add(conexionA);
			predefinido.add(conexionB);
			predefinido.add(conexionC);
		}

		if(tipo.equals("Ordenado"))
		{
			predefinido.add(conexionA);
			predefinido.add(conexionC);
			predefinido.add(conexionB);
		}
		return predefinido;
	}
}
