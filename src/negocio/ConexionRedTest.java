package negocio;

import static org.junit.Assert.*;

import org.junit.Test;

public class ConexionRedTest {

	@Test
	public void happyPathTest()
	{
		Localidad localidad1 = new Localidad("Don Torcuato", "Buenos Aires", 10000, -34.49, -58.62);
		Localidad localidad2 = new Localidad ("Benavidez", "Buenos aires", 25000, -34.41, -58.68);
	
		ConexionRed conexionA = new ConexionRed(localidad1, localidad2);
		ConexionRed conexionB = new ConexionRed(localidad2, localidad1);

		assertFalse(conexionA.equals(conexionB));
		assertFalse(conexionB.equals(conexionA));
	}
	
	@Test
	public void PesoConexionesTest()
	{
		Localidad localidad1 = new Localidad("Don Torcuato", "Buenos Aires", 10000, -34.49, -58.62);
		Localidad localidad2 = new Localidad ("Benavidez", "Buenos aires", 25000, -34.41, -58.68);

		ConexionRed conexionA = new ConexionRed(localidad1, localidad2);
		ConexionRed conexionB = new ConexionRed(localidad2, localidad1);

		assertTrue(conexionA.getPeso() == conexionB.getPeso());
	}
	
	@Test
	public void LocalidadesIgualesTest()
	{
		Localidad localidad1 = new Localidad("Don Torcuato", "Buenos Aires", 10000, -34.49, -58.62);
		Localidad localidad2 = new Localidad("Don Torcuato", "Buenos Aires", 10000, -34.49, -58.62);

		ConexionRed conexion = new ConexionRed(localidad1, localidad2);

		assertNotSame(conexion.getOrigen(), conexion.getDestino());
	}
}