package negocio;

import java.util.ArrayList;

public class Negocio
{	
	private RedTelefonica redTelefonica;
	private AlgoritmoKruskal AGM;
	private ArrayList<ConexionRed> redOptima;
	private double costo;
	
	public Negocio()
	{
		redTelefonica = new RedTelefonica();
		AGM = new AlgoritmoKruskal(redTelefonica);
		redOptima = redTelefonica.getTodasConexiones();
		costo = 0;
	}

	public RedTelefonica getRedTelefonica()
	{
		return redTelefonica;
	}

	public ArrayList<ConexionRed> getRedOptima()
	{
		return redOptima;
	}
	
	public double getCosto(){
		return costo;
	}

	public void botonAgregarLocalidad(String n, String p, int h, float lat, float lon)
	{
		redTelefonica.agregarLocalidad(n, p, h, lat, lon);
		redTelefonica.TCT();
	}

	public void botonGenerar()
	{
		redOptima = AGM.kruskal();
	}

	public void botonCostos()
	{
		for (ConexionRed conexionRed : redOptima) {
			costo = costo + conexionRed.getPeso();
		}
	}
}