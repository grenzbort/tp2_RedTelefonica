package negocio;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import org.junit.Test;

public class CostosTest {

	@Test
	public void pesoMayorA200Test()
	{
		ConexionRed conexion = cargarConexion(0);
		
		assertTrue(Costos.doscientosKM(conexion.getOrigen(), conexion.getDestino()));
	}
	
	@Test
	public void distintasProvinciasTest()
	{
		ConexionRed conexion = cargarConexion(0);
		
		assertTrue(Costos.provinciasDistintas(conexion.getOrigen(), conexion.getDestino()));
	}
	
	@Test
	public void mismaProvinciaYMayor200KMTest()
	{
		ConexionRed conexion = cargarConexion(2);
		
		assertFalse(Costos.provinciasDistintas(conexion.getOrigen(), conexion.getDestino()));
		assertTrue(Costos.doscientosKM(conexion.getOrigen(), conexion.getDestino()));
	}
	
	@Test
	public void distintasProvinciasYMayor200KMTest()
	{
		ConexionRed conexion = cargarConexion(0);
		
		assertTrue(Costos.provinciasDistintas(conexion.getOrigen(), conexion.getDestino()));
		assertTrue(Costos.doscientosKM(conexion.getOrigen(), conexion.getDestino()));
	}
	
	@Test
	public void calcularCostoTest()
	{
		ConexionRed conexion = cargarConexion(0);
		
		double costo = Costos.calcularPeso(conexion.getOrigen(), conexion.getDestino());
		
		BigDecimal esperado = new BigDecimal(753.50);		
		BigDecimal bd = new BigDecimal(costo);
		bd = bd.setScale(1, RoundingMode.HALF_UP);
		
		assertEquals(esperado, bd);
	}
	
	private ConexionRed cargarConexion(int indice) {
		ArrayList<ConexionRed> conexiones = inicializarConexiones();		
		ConexionRed conexion = conexiones.get(indice);
		return conexion;
	}

	private ArrayList<ConexionRed> inicializarConexiones()
	{
		Localidad localidad1 = new Localidad("Don Torcuato", "Buenos Aires", 10000, -34.49, -58.62);
		Localidad localidad2 = new Localidad ("Rosario", "Santa Fe", 125000, -32.94, -60.63);
		Localidad localidad3 = new Localidad("Bahia Blanca", "Buenos Aires", 250000, -38.71, -62.24);
		
		ConexionRed conexionA = new ConexionRed (localidad1, localidad2);
		ConexionRed conexionB = new ConexionRed (localidad2, localidad3);
		ConexionRed conexionC = new ConexionRed (localidad3, localidad1);
		
		ArrayList<ConexionRed> conexiones = new ArrayList<ConexionRed>();
		
		conexiones.add(conexionA);
		conexiones.add(conexionB);
		conexiones.add(conexionC);
		
		return conexiones;
	}
}
