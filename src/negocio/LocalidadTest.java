package negocio;

import static org.junit.Assert.*;

import org.junit.Test;

public class LocalidadTest {

	@Test
	public void localidadesDiferentesTest()
	{
		Localidad localidad1 = new Localidad("Don Torcuato", "Buenos Aires", 10000, -34.49, -58.62);
		Localidad localidad2 = new Localidad ("Benavidez", "Buenos aires", 25000, -34.41, -58.68);
	
		assertFalse(localidad1.equals(localidad2));
	}
}
