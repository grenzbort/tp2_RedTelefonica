package negocio;

public class ConexionRed implements Comparable<ConexionRed>
{
	private Localidad origen;
	private Localidad destino;
	private double peso;

	public ConexionRed(Localidad origen, Localidad destino)
	{
		this.origen = origen;
		this.destino = destino;
		peso = Costos.calcularPeso(origen, destino);
	}
	
	public double getPeso()
	{
		return peso;
	}

	public Localidad getOrigen()
	{
		return origen;
	}

	public Localidad getDestino()
	{
		return destino;
	}
	
	@Override
	public String toString()
	{
		return "(" + origen + ", " + destino + ", " + peso + ")";
	}

	@Override
	public int compareTo(ConexionRed a)
	{	 
        if (peso < a.getPeso())
        {
        	return -1;
        }
        else if(peso == a.getPeso())
        {
        	return 0;  
        }
        else
        {
        	return 1; 
        }
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((destino == null) ? 0 : destino.hashCode());
		result = prime * result + ((origen == null) ? 0 : origen.hashCode());
		long temp;
		temp = Double.doubleToLongBits(peso);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConexionRed other = (ConexionRed) obj;
		if (destino == null) {
			if (other.destino != null)
				return false;
		} else if (!destino.equals(other.destino))
			return false;
		if (origen == null) {
			if (other.origen != null)
				return false;
		} else if (!origen.equals(other.origen))
			return false;
		if (Double.doubleToLongBits(peso) != Double
				.doubleToLongBits(other.peso))
			return false;
		return true;
	}
}