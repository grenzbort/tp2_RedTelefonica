package negocio;

import java.util.ArrayList;

public class AlgoritmoKruskal
{
	RedTelefonica grafo;
	
	public AlgoritmoKruskal(RedTelefonica red)
	{
		this.grafo = red;
	}
	
	public ArrayList<ConexionRed> kruskal()
	{
		if(grafo.cantLocalidadesTotales() == 0)
		{
			return new ArrayList<ConexionRed>();
		}
		return aplicarKruskal();
	}
	
	private ArrayList<ConexionRed> aplicarKruskal()
	{	
		ArrayList<ConexionRed> AGM = new ArrayList<ConexionRed>();		
		ArrayList<ConexionRed> todasAristas = grafo.getTodasConexiones();
		
		MergeSort<ConexionRed> ordenarConexiones = new MergeSort<ConexionRed>(todasAristas);
			
		ordenarConexiones.mergesort();
		
		while(AGM.size() < grafo.cantLocalidadesTotales()-1 && !todasAristas.isEmpty())
		{	
			ConexionRed actual = todasAristas.get(0); 
			
			if ( !hayCircuito(AGM, actual) )
			{
				AGM.add(actual);
			}

			todasAristas.remove(actual);
		}
		return AGM;
	}
	
	//Metodo que verifica si se forma un ciclo al agregar una conexionRed
	@SuppressWarnings("unchecked")
	private boolean hayCircuito(ArrayList<ConexionRed> arbolAGM, ConexionRed candidato)
	{
		if(arbolAGM.size() == 0)
		{
			return false;
		}

		if(arbolAGM.contains(candidato)) 
		{
			return true;
		}
		
		if(idaYvuelta(arbolAGM, candidato))
		{
			return true;
		}
		
		if(!aristaSeparada(arbolAGM, candidato))
		{
			return false;
		}
		
		ArrayList<ConexionRed> arbolAux = (ArrayList<ConexionRed>) arbolAGM.clone();		
		ArrayList<Localidad> vecinosVisitados = new ArrayList<Localidad> ();
		arbolAux.add(candidato);

		if (hayCiclo(arbolAux, vecinosVisitados, candidato, candidato.getDestino()))
		{
			return true;			
		}
		return false;		
	}

	private boolean hayCiclo(ArrayList<ConexionRed> grafo, ArrayList<Localidad> vecinosVisitados, ConexionRed candidato, Localidad localidadActual)
	{	
		if (localidadActual.equals(candidato.getOrigen()))
		{
			return true;
		}

		ArrayList<Localidad> vecinos = new ArrayList<Localidad>(); 
		boolean check = false;

		if (!vecinosVisitados.contains(localidadActual))
		{
			vecinosVisitados.add(localidadActual);
			vecinos = pedirVecinos(grafo, vecinosVisitados, localidadActual, candidato);
		}
		
		if(vecinos.size() == 0)
		{
			return false;
		}else{			
			for (int i = 0; i < vecinos.size(); i++)
			{
				check = check || hayCiclo(grafo, vecinosVisitados, candidato, vecinos.get(i));
			}
		}
		return check;
	}		

	private ArrayList<Localidad> pedirVecinos(ArrayList<ConexionRed> grafo, ArrayList<Localidad> vecinosVisitados, Localidad localidadActual, ConexionRed candidato)
	{
		ArrayList<Localidad> localidadesVecinas = new ArrayList<Localidad>();

		for (ConexionRed conexionRed : grafo)
		{
			if(conexionRed.equals(candidato))
			{
				continue;
			}
			
			if(!vecinosVisitados.contains(conexionRed.getOrigen()))
			{
				if (conexionRed.getDestino().equals(localidadActual))
				{
					localidadesVecinas.add(conexionRed.getOrigen());
				}				
			}
			if(!vecinosVisitados.contains(conexionRed.getDestino()))
			{
				if (conexionRed.getOrigen().equals(localidadActual)) 
				{
					localidadesVecinas.add(conexionRed.getDestino());
				}			
			}
		}
		return localidadesVecinas;

	}

	private boolean aristaSeparada(ArrayList<ConexionRed> arbolAGM, ConexionRed candidato) {

		boolean ret = false;
		for (ConexionRed conexionRed : arbolAGM)
		{
			if (candidato.getOrigen().equals(conexionRed.getOrigen()) || candidato.getOrigen().equals(conexionRed.getDestino()) && 
				candidato.getDestino().equals(conexionRed.getOrigen()) || candidato.getDestino().equals(conexionRed.getDestino()))
			{
				ret = true;
			}
		}
		return ret;
	}
	
	private boolean idaYvuelta(ArrayList<ConexionRed> arbolAGM, ConexionRed candidato)
	{
		for (ConexionRed conexionRed : arbolAGM)
		{
			if (candidato.getDestino().equals(conexionRed.getOrigen()) &&
				(candidato.getOrigen().equals(conexionRed.getDestino())))
			{
				return true;
			}
		}
		return false;
	}
}