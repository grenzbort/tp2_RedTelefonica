package negocio;

import java.util.ArrayList;

import static org.junit.Assert.*;
import org.junit.Test;

public class AlgoritmoKruskalTest
{

	@Test
	public void happyPathTest()
	{
		RedTelefonica red = RedTodosContraTodos();
		AlgoritmoKruskal agm = new AlgoritmoKruskal(red);
		ArrayList<ConexionRed> mejor = agm.kruskal();
		
		assertTrue(mejor.size() == red.cantLocalidadesTotales()-1);
	}
	
	@Test
	public void RedVaciaTest()
	{
		RedTelefonica red = new RedTelefonica();
		AlgoritmoKruskal agm = new AlgoritmoKruskal(red);
		ArrayList<ConexionRed> mejor = agm.kruskal();
		
		assertEquals(mejor.size(), red.cantLocalidadesTotales());
	}
	
	@Test
	public void RedCompletaSinAristasTest()
	{
		RedTelefonica red = RedSinAristas();
		AlgoritmoKruskal agm = new AlgoritmoKruskal(red);
		ArrayList<ConexionRed> mejor = agm.kruskal();
		
		assertEquals(0, mejor.size());
	}
	
	@Test
	public void UnicaAristaTest()
	{
		RedTelefonica red = RedSinAristas();
		
		Localidad origen = red.getLocalidadEspecifica("Don Torcuato");
		Localidad destino = red.getLocalidadEspecifica("Pacheco");
		
		red.agregarConexion(origen, destino);
		
		AlgoritmoKruskal agm = new AlgoritmoKruskal(red);
		ArrayList<ConexionRed> mejor = agm.kruskal();
		
		assertEquals(1, mejor.size());
	}
	
	public RedTelefonica RedTodosContraTodos()
	{
		RedTelefonica red = inicializarRed();	

		red.TCT();

		return red;
	}
	
	public RedTelefonica RedSinAristas()
	{
		RedTelefonica red = inicializarRed();	

		return red;
	}	
	
	private RedTelefonica inicializarRed()
	{
		RedTelefonica red = new RedTelefonica();
		
		red.agregarLocalidad("Don Torcuato" ,"Buenos Aires", 20000 , 20 , 30);
		red.agregarLocalidad("Pacheco" ,"Buenos Aires", 20000 , 20 , 37);
		red.agregarLocalidad("Benavides" ,"Buenos Aires", 20000 , 20 , 31);
		red.agregarLocalidad("Tigre" ,"Buenos Aires", 20000 , 20 , 14);
		red.agregarLocalidad("San Isidro" ,"Buenos Aires", 20000 , 21 , 14);
		red.agregarLocalidad("Beccar" ,"Buenos Aires", 20000 , 22 , 16);
		red.agregarLocalidad("Capital" ,"Buenos Aires", 20000 , 25, 30);
		red.agregarLocalidad("Polvorines" ,"Buenos Aires", 20000 , 25 , 14);
		return red;
	}	
}