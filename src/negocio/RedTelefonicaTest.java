package negocio;

import static org.junit.Assert.*;

import org.junit.Test;

public class RedTelefonicaTest
{
	@Test
	public void getLocalidadEspecificaTest()
	{
		RedTelefonica red = cargarLocalidades();
		Localidad DT = new Localidad ("Don Torcuato" ,"Buenos Aires", 20000 , 20 , 30);
		
		assertTrue(red.getLocalidadEspecifica("Don Torcuato").equals(DT));;
	}

	@Test
	public void cargarLocalidadTest()
	{
		RedTelefonica red = new RedTelefonica();
		red.agregarLocalidad("tigre", "buenos aires", 15, 15, 32);
		
		assertEquals(1, red.cantLocalidadesTotales());
	}

	@Test
	public void getCantidadLocalidadTest()
	{
		RedTelefonica red = cargarLocalidades();
		
		assertEquals(4, red.cantLocalidadesTotales());
	}
	
	@Test
	public void getCantidadConexionesVaciaTest()
	{
		RedTelefonica red = cargarLocalidades();
		
		assertEquals(0, red.cantConexionesTotales());
	}
	
	@Test
	public void getCantidadConexionesCompletaTest()
	{
		RedTelefonica red = cargarLocalidades();
		red.TCT();
		
		assertEquals(12, red.cantConexionesTotales());
	}
	
	@Test
	public void getCantidadConexionesDeLocalidadEspecificaTest()
	{
		RedTelefonica red = cargarLocalidades();
		red.TCT();
		
		Localidad benavidez = red.getLocalidadEspecifica("Benavidez");
		
		assertEquals(3, red.getConexionesLocalidadEspecifica(benavidez).size());
	}
	
	private RedTelefonica cargarLocalidades()
	{
		RedTelefonica red = new RedTelefonica();
		
		red.agregarLocalidad("Don Torcuato" ,"Buenos Aires", 20000 , 20 , 30);
		red.agregarLocalidad("Pacheco" ,"Buenos Aires", 20000 , 20 , 37);
		red.agregarLocalidad("Benavidez" ,"Buenos Aires", 20000 , 20 , 31);
		red.agregarLocalidad("Tigre" ,"Buenos Aires", 20000 , 20 , 14);
		
		return red;
	}
}
