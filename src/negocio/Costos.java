package negocio;

public class Costos
{

	public static double calcularPeso(Localidad origen, Localidad destino)
	{	
		double peso = 0;

		peso += distanciaEntre(origen, destino);

		if (provinciasDistintas(origen, destino)) 
		{
			peso += 300;
		}
		if (doscientosKM(origen, destino)) 
		{
			peso += 200;
		}

		return peso;	
	}

	static boolean provinciasDistintas(Localidad origen, Localidad destino) 
	{
		if (!origen.getProvincia().equals(destino.getProvincia())) 
		{
			return true;
		}
		return false;
	}

	static boolean doscientosKM(Localidad origen, Localidad destino)
	{
		if (distanciaEntre(origen, destino) > 200)
		{
			return true;
		}
		return false;
	}

	public static double distanciaEntre(Localidad origen, Localidad destino)
	{
		double radioTierra = 6371;
		double dLat = Math.toRadians(origen.getLatitud() - destino.getLatitud());
		double dLng = Math.toRadians(origen.getLongitud() - destino.getLongitud());
		double sindLat = Math.sin(dLat / 2);
		double sindLng = Math.sin(dLng / 2);
		double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2) * Math.cos(Math.toRadians(origen.getLatitud())) * Math.cos(Math.toRadians(destino.getLatitud()));
		double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
		double distancia = radioTierra * va2;

		return distancia;
	}

	int stringToInt(String cadena)
	{
		try
		{
			return Integer.parseInt(cadena);
		} 
		catch (Exception ex)
		{
			return 0;
		}
	}

	double stringToDouble(String cadena)
	{
		try
		{
			double ret = stringToInt(cadena);
			return ret;
		}
		catch(Exception ex) {
			return 0;
		}
	}

}