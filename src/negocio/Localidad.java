package negocio;

public class Localidad
{
	private String nombre, provincia;
	private int habitantes;
	private double latitud;
	private double longitud;

	public Localidad(String nom, String prov, int hab, double lat, double lon)
	{
		nombre = nom;
		provincia = prov;
		habitantes = hab;
		latitud = lat;
		longitud = lon;
	}

	public String getNombre()
	{
		return nombre;
	}
	
	public String getProvincia()
	{
		return provincia;
	}
	
	public int getHabitantes()
	{
		return habitantes;
	}
	
	public double getLatitud()
	{
		return latitud;
	}
	
	public double getLongitud()
	{
		return longitud;
	}
	
	@Override
	public String toString()
	{
		return "" + nombre;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + habitantes;
		long temp;
		temp = Double.doubleToLongBits(latitud);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(longitud);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result
				+ ((provincia == null) ? 0 : provincia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Localidad other = (Localidad) obj;
		if (habitantes != other.habitantes)
			return false;
		if (Double.doubleToLongBits(latitud) != Double
				.doubleToLongBits(other.latitud))
			return false;
		if (Double.doubleToLongBits(longitud) != Double
				.doubleToLongBits(other.longitud))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (provincia == null) {
			if (other.provincia != null)
				return false;
		} else if (!provincia.equals(other.provincia))
			return false;
		return true;
	}
}