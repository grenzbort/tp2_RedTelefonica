package negocio;

import java.util.ArrayList;

class AuxAssert
{
	public static boolean ArrayEquals(ArrayList<ConexionRed> esperado, ArrayList<ConexionRed> array)
	{
		boolean iguales = true;

		if (esperado.size() == array.size())
		{		
			for (int i = 0; i < esperado.size() ; i++)
			{
				if(esperado.get(i).equals(array.get(i)))
				{
					iguales = iguales && true;
				}else{
					iguales = false;					
				}
			}
		}else{
			iguales = false;
		}	
		return iguales;
	}

}