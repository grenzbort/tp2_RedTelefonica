package negocio;

import java.util.ArrayList;
import java.util.HashMap;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Utilidades {
	
	public boolean verificarCampos(String localidad, String provincia, String habitantes, String latitud, String longitud)
	{
		if ((localidad.trim().length() == 0) ||	(provincia.trim().length() == 0) ||
			(habitantes.trim().length() == 0) || (latitud.trim().length() == 0) ||
			(longitud.trim().length() == 0))
		{
			return false;
		}
		return true;
		
	}
	
	public int stringToInt(String respuesta)
	{
		try
		{
			return Integer.parseInt(respuesta);
		}
		catch(Exception ex)
		{
			return 0;
		}
	}
	
	public float stringToFloat(String respuesta)
	{
		try
		{
			return Float.parseFloat(respuesta);
		}
		catch(Exception ex)
		{
			return 0;
		}
	}
	
	public HashMap<Coordinate, Coordinate> coordenadas(ArrayList<ConexionRed> redOptima)
	{
		HashMap<Coordinate, Coordinate> ret = new HashMap<>();
		for(ConexionRed e : redOptima)
		{
			Coordinate origen = new Coordinate(e.getOrigen().getLatitud(), e.getOrigen().getLongitud());
			Coordinate destino = new Coordinate(e.getDestino().getLatitud(), e.getDestino().getLongitud());
			ret.put(origen,destino);
		}
		return ret;
	}	
}
