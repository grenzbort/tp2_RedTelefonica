package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import java.awt.Font;
import java.awt.Color;

import negocio.*;

import javax.swing.ImageIcon;
import javax.swing.border.LineBorder;
import javax.swing.SwingConstants;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;

public class InterfazGrafica {
	
	private JFrame frmAgm;
	private JTextField localidadUsuario;
	private JTextField provinciaUsuario;
	private JTextField habitantesUsuario;
	private JTextField latitudUsuario;
	private JTextField longitudUsuario;

	private Negocio negocio = new Negocio();
	private Utilidades utilidades = new Utilidades();
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazGrafica window = new InterfazGrafica();
					window.frmAgm.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	} 
	
	public InterfazGrafica() {
		initialize();
	}

	private void initialize() {

		frmAgm = new JFrame();
		frmAgm.setTitle("Red Telefonica");
		frmAgm.setResizable(false);
		frmAgm.setBounds(100, 100, 1002, 662);
		frmAgm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAgm.getContentPane().setLayout(null);

		JMapViewer JMapa = new JMapViewer();
		JMapa.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		JMapa.setBounds(441, 11, 545, 440);
		frmAgm.getContentPane().add(JMapa);
		JMapa.setZoomContolsVisible(false);
		JMapa.setDisplayPositionByLatLon(-34.520472, -58.699889, 11);
		JMapa.setLayout(null);
		
		JLabel gastos = new JLabel("");
		gastos.setFont(new Font("Tahoma", Font.PLAIN, 17));
		gastos.setBounds(20, 379, 401, 167);
		gastos.setHorizontalAlignment(SwingConstants.CENTER);
		frmAgm.getContentPane().add(gastos);
		
		JLabel tituloCostos = new JLabel("COSTOS");
		tituloCostos.setFont(new Font("Tahoma", Font.PLAIN, 22));
		tituloCostos.setHorizontalAlignment(SwingConstants.CENTER);
		tituloCostos.setBounds(10, 368, 421, 44);
		frmAgm.getContentPane().add(tituloCostos);

		JLabel generarAGM = new JLabel("");
		generarAGM.setToolTipText("Generar red telef\u00F3nica (AGM) y mostrar por pantalla.");
		generarAGM.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				JMapa.getMapPolygonList().clear();
				negocio.botonGenerar();
				generarConexion(JMapa);
			}
			@Override
			public void mouseEntered(MouseEvent arg0) {
				generarAGM.setIcon(new ImageIcon(InterfazGrafica.class.getResource("/interfaz/imagenes/generarAGM_remarcado.png")));
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				generarAGM.setIcon(new ImageIcon(InterfazGrafica.class.getResource("/interfaz/imagenes/generarAGM.png")));
			}
		});
		generarAGM.setIcon(new ImageIcon(InterfazGrafica.class.getResource("/interfaz/imagenes/generarAGM.png")));
		generarAGM.setBounds(600, 482, 100, 100);
		frmAgm.getContentPane().add(generarAGM);
		
		JLabel calcularCosto = new JLabel("");
		calcularCosto.setToolTipText("Conocer costos de la red telef\u00F3nica.");
		calcularCosto.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				negocio.botonCostos();
				String costo = String.valueOf(negocio.getCosto());
				gastos.setText("<html>El costo para instalar esta Red Telefonica es de:<br>$"+costo+" pesos.</html>");
			}
			@Override
			public void mouseEntered(MouseEvent arg0) {
				calcularCosto.setIcon(new ImageIcon(InterfazGrafica.class.getResource("/interfaz/imagenes/calcularCostos_remarcado.png")));
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				calcularCosto.setIcon(new ImageIcon(InterfazGrafica.class.getResource("/interfaz/imagenes/calcularCostos.png")));
			}
		});
		calcularCosto.setIcon(new ImageIcon(InterfazGrafica.class.getResource("/interfaz/imagenes/calcularCostos.png")));
		calcularCosto.setBounds(726, 482, 100, 100);
		frmAgm.getContentPane().add(calcularCosto);
		
		/*JPanel panel_costos = new JPanel();
		panel_costos.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		panel_costos.setBackground(new Color(255, 222, 173));
		panel_costos.setBounds(10, 367, 421, 252);
		frmAgm.getContentPane().add(panel_costos);
		panel_costos.setLayout(null);*/
		
		JPanel panel_localidad = new JPanel();
		panel_localidad.setBackground(new Color(255, 222, 173));
		panel_localidad.setBorder(new LineBorder(new Color(0, 0, 0), 3, true));
		panel_localidad.setBounds(10, 11, 421, 346);
		frmAgm.getContentPane().add(panel_localidad);
		panel_localidad.setLayout(null);
		
		JLabel tituloAgregar = new JLabel("AGREGAR LOCALIDAD");
		tituloAgregar.setBackground(Color.YELLOW);
		tituloAgregar.setHorizontalAlignment(SwingConstants.CENTER);
		tituloAgregar.setFont(new Font("Tahoma", Font.PLAIN, 22));
		tituloAgregar.setBounds(0, 5, 421, 44);
		panel_localidad.add(tituloAgregar);

		localidadUsuario = new JTextField();
		localidadUsuario.setToolTipText("Nombre de la localidad.");
		localidadUsuario.setBounds(170, 55, 240, 30);
		panel_localidad.add(localidadUsuario);
		localidadUsuario.setFont(new Font("Tahoma", Font.PLAIN, 20));
		localidadUsuario.setColumns(10);

		JLabel lbl_Localidad = new JLabel("LOCALIDAD:");
		lbl_Localidad.setBounds(20, 55, 112, 25);
		panel_localidad.add(lbl_Localidad);
		lbl_Localidad.setFont(new Font("Tahoma", Font.PLAIN, 20));

		provinciaUsuario = new JTextField();
		provinciaUsuario.setToolTipText("Nombre de la provincia.");
		provinciaUsuario.setBounds(170, 90, 240, 30);
		panel_localidad.add(provinciaUsuario);
		provinciaUsuario.setFont(new Font("Tahoma", Font.PLAIN, 20));
		provinciaUsuario.setColumns(10);

		JLabel lbl_Provincia = new JLabel("PROVINCIA:");
		lbl_Provincia.setBounds(20, 90, 107, 25);
		panel_localidad.add(lbl_Provincia);
		lbl_Provincia.setFont(new Font("Tahoma", Font.PLAIN, 20));

		habitantesUsuario = new JTextField();
		habitantesUsuario.setToolTipText("Cantidad de habitantes.");
		habitantesUsuario.setBounds(170, 125, 240, 30);
		panel_localidad.add(habitantesUsuario);
		habitantesUsuario.setFont(new Font("Tahoma", Font.PLAIN, 20));
		habitantesUsuario.setColumns(10);
		habitantesUsuario.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
				esNumero(e, habitantesUsuario);
			}
		});

		JLabel lbl_habitantes = new JLabel("HABITANTES:");
		lbl_habitantes.setBounds(20, 125, 123, 25);
		panel_localidad.add(lbl_habitantes);
		lbl_habitantes.setFont(new Font("Tahoma", Font.PLAIN, 20));

		latitudUsuario = new JTextField();
		latitudUsuario.setBounds(170, 160, 240, 30);
		panel_localidad.add(latitudUsuario);
		latitudUsuario.setFont(new Font("Tahoma", Font.PLAIN, 20));
		latitudUsuario.setColumns(10);
		latitudUsuario.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
				esNumero(e, latitudUsuario);
			}
		});

		JLabel lbl_latitud = new JLabel("LATITUD:");
		lbl_latitud.setBounds(20, 160, 87, 25);
		panel_localidad.add(lbl_latitud);
		lbl_latitud.setFont(new Font("Tahoma", Font.PLAIN, 20));

		longitudUsuario = new JTextField();
		longitudUsuario.setBounds(170, 195, 240, 30);
		panel_localidad.add(longitudUsuario);
		longitudUsuario.setFont(new Font("Tahoma", Font.PLAIN, 20));
		longitudUsuario.setColumns(10);
		longitudUsuario.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
				esNumero(e, longitudUsuario);
			}
		});

		JLabel lbl_longitud = new JLabel("LONGITUD:");
		lbl_longitud.setBounds(20, 195, 103, 25);
		panel_localidad.add(lbl_longitud);
		lbl_longitud.setFont(new Font("Tahoma", Font.PLAIN, 20));
		
		JLabel guardarLocalidad = new JLabel("");
		guardarLocalidad.setToolTipText("Agregar localidad.");
		guardarLocalidad.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				String localidad = localidadUsuario.getText(); 
				String provincia = provinciaUsuario.getText();
				
				if (utilidades.verificarCampos(localidad, provincia, habitantesUsuario.getText(), latitudUsuario.getText(), longitudUsuario.getText()))
				{
					int habitantes = utilidades.stringToInt(habitantesUsuario.getText());
					float latitud = utilidades.stringToFloat(latitudUsuario.getText());
					float longitud = utilidades.stringToFloat(longitudUsuario.getText());
					negocio.botonAgregarLocalidad(localidad, provincia, habitantes, latitud, longitud);
					marcarPunto(JMapa, localidad, latitud, longitud);
					JOptionPane.showMessageDialog(null,"La localidad "+"'"+localidad+"'"+" fue agregada correctamente.");
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Un campo no fue ingresado!");
				}
			}
			@Override
			public void mouseEntered(MouseEvent arg0) {
				guardarLocalidad.setIcon(new ImageIcon(InterfazGrafica.class.getResource("/interfaz/imagenes/guardarLocalidad_remarcado.png")));
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				guardarLocalidad.setIcon(new ImageIcon(InterfazGrafica.class.getResource("/interfaz/imagenes/guardarLocalidad.png")));
			}
		});
		guardarLocalidad.setIcon(new ImageIcon(InterfazGrafica.class.getResource("/interfaz/imagenes/guardarLocalidad.png")));
		guardarLocalidad.setBounds(160, 235, 100, 100);
		panel_localidad.add(guardarLocalidad);
		
		JLabel fondo = new JLabel("");
		fondo.setIcon(new ImageIcon(InterfazGrafica.class.getResource("/interfaz/imagenes/fondoPantalla.png")));
		fondo.setBounds(0, 0, 1000, 635);
		frmAgm.getContentPane().add(fondo);
	}
	
	private void esNumero(KeyEvent e, JTextField campo) 
	{
		char caracter = e.getKeyChar(); 
		if (((caracter < '0') || (caracter > '9')) 
		        && (caracter != KeyEvent.VK_BACK_SPACE)
		        && (caracter != '-' || campo.getText().contains("-"))
		        && (caracter != '.' || campo.getText().contains(".")) ) 
		{
		            e.consume();
		            JOptionPane.showMessageDialog(null, "solo se admiten numeros en este campo.");
		}
	}
	
	private void marcarPunto(JMapViewer JMapa, String localidad, float latitud, float longitud)
	{
		MapMarker marker = new MapMarkerDot(null,"  "+localidad,latitud,longitud);
		marker.getStyle().setBackColor(Color.YELLOW);
		JMapa.addMapMarker(marker);
	}
	
	private void generarConexion(JMapViewer JMapa)
	{
		HashMap<Coordinate, Coordinate> coordenadas = utilidades.coordenadas(negocio.getRedOptima());
		for(Coordinate coord : coordenadas.keySet()) 
		{
			ArrayList<Coordinate> coordenadas_2 = new ArrayList<>();
			coordenadas_2.add(coord);
			coordenadas_2.add(coordenadas.get(coord));
			coordenadas_2.add(coord);
			
			MapPolygon polygon = new MapPolygonImpl("", coordenadas_2);
			polygon.getStyle().setColor(Color.MAGENTA);
			JMapa.addMapPolygon(polygon);
			JMapa.repaint();
		}
	}
}
